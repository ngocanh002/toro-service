import { Injectable } from '@angular/core';

// tslint:disable-next-line:class-name
export declare interface getUser {
    ngGetUserAll(): void;

    ngGetById(id: number);
}
