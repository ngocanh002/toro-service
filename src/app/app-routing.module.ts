import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './pages/admin-pages/home/home.component';
import { AuthGuard } from './helpers/auth.guard';
import { DashboardManagerComponent } from './pages/admin-pages/dashboard-manager/dashboard-manager.component';

const routes: Routes = [

  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },

  { path: 'login', component: LoginComponent},

  { path: '', redirectTo: 'login:', pathMatch: 'full' },

  { path: 'register', component: RegisterComponent },

  { path: 'manager', component: DashboardManagerComponent, canActivate: [AuthGuard]},


  // otherwise redirect to home
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
