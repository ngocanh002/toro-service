import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, pipe } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ApiService } from './api.service';
import { ConfigUrlService } from './config-url.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  apiUrl: 'http://localhost:4000';
  private currentUserSubject: BehaviorSubject<any>;

  public currentUser: Observable<any>;


  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  login(username, password) {
    return this.apiService.post(this.configUrlService.auth, { username, password })
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    // remove user form local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }



}
