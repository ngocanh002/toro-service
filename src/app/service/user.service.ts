import { Injectable } from '@angular/core';
import { ConfigUrlService } from './config-url.service';
import { ApiService } from './api.service';

@Injectable()
export class UserService {

    constructor(
        private configUrlService: ConfigUrlService,
        private apiService: ApiService
    ) {

    }
    getUserAll() {
        return this.apiService.get(this.configUrlService.getAllUser);
    }

    ngGetById(id: number) {

    }

}
