import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  getErrorMessage(account) {
    return account.hasError('required') ? 'Bạn chưa nhập tài khoản' : '';
  }
}
