import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private user: any;
  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
  ) { }

  ngOnInit() {
    // x
  }

  // get name() {
  //   return this.authenticationService.currentUser.subscribe(data => this.user = data);
  // }

  getUser() {
    this.userService.getUserAll().subscribe(data => {
      console.log(data);
    });
  }
}
