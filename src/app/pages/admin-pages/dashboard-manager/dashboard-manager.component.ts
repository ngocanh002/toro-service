import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-dashboard-manager',
  templateUrl: './dashboard-manager.component.html',
  styleUrls: ['./dashboard-manager.component.css']
})
export class DashboardManagerComponent implements OnInit {

  constructor(private http: HttpClient) { }
  data = {};
  ngOnInit() {
    this.getData();
  }


  getData() {
    this.http.get('http://127.0.0.1:3000/users').subscribe(data => {
      this.data = data;
      console.log(data);
    });
  }





}
