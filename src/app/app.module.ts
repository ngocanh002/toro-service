import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AdminHeaderComponent } from './components/admin-component/admin-header/admin-header.component';
import { NotFoundComponent } from './components/admin-component/not-found/not-found.component';
import { AdminFooterComponent } from './components/admin-component/admin-footer/admin-footer.component';
import { AdminSidebarComponent } from './components/admin-component/admin-sidebar/admin-sidebar.component';
import { AdminNavbarComponent } from './components/admin-component/admin-navbar/admin-navbar.component';

// form
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ApiConfig } from './configs/api.config';
import { LoginComponent } from './pages/login/login.component';
// import material
import { MaterialModule } from './material';
//
import { ConfigUrlService } from './service/config-url.service';
import { ApiService } from './service/api.service';
import { RegisterComponent } from './pages/register/register.component';


// user crate fake backenk
import { HeaderInterceptorProvider } from './helpers/fake-backend';
import { HomeComponent } from './pages/admin-pages/home/home.component';
import { AuthGuard } from './helpers/auth.guard';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { UserService } from './service/user.service';
import { RawOptionsCallbacks, RawOptions } from 'filterizr/dist/types/interfaces';
import { filter } from 'rxjs/operators';
import { DashboardManagerComponent } from './pages/admin-pages/dashboard-manager/dashboard-manager.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminHeaderComponent,
    AdminNavbarComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardManagerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    // admin lte
    // modele material
    MaterialModule
  ],
  providers: [
    ApiConfig,
    ConfigUrlService,
    ApiService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    // AuthGuard,
    // back end
    HeaderInterceptorProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
